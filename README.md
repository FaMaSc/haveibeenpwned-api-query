# haveibeenpwned-api-query

Simple Python script that accepts a CSV with email addresses and queries the **Have I Been Pwned** API to check if any of them are comprimised.

https://haveibeenpwned.com/API/v3

An API key is required to run the script: https://haveibeenpwned.com/API/Key

Because the API has a rate limiting, a delay of 2 seconds has to be used after each single query.