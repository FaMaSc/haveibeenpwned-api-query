import csv
import json
import requests
import time

hibp_base_url = 'https://haveibeenpwned.com/api/v3/'
hibp_breaches = hibp_base_url + 'breachedaccount/'
hibp_pastes = hibp_base_url + 'pasteaccount/'

headers = {'hibp-api-key': '<api-key>',     # insert your api key
             'user-agent': 'my-user-agent'} # specify your user agent
      
count_total = 0
count_pwned = 0

with open('myEmailAddresses.csv', newline='') as csvfile:    # specify your input csv

    csvreader = csv.reader(csvfile, delimiter=';')
    for row in csvreader:

        user = row[0].lower()   # specify column with email address
        count_total += 1
        match = False

        # test address for breach
        url_breach_user = '{0}{1}'.format(hibp_breaches, user)
        response = requests.get(url_breach_user, headers=headers)

        if response.status_code == 200:
            match = True
            print('BREACH DETECTED - {0}'.format(user))
        elif response.status_code != 404:
            print('{0} - {1} - breach unknown status code'.format(response.status_code, user))

        time.sleep(2)

        # test address for paste
        url_paste_user = '{0}{1}'.format(hibp_pastes, user)
        response = requests.get(url_paste_user, headers=headers)

        if response.status_code == 200:
            match = True
            print('PASTE DETECTED - {0}'.format(user))
        elif response.status_code != 404:
            print('{0} - {1} - paste unknown status code'.format(response.status_code, user))

        time.sleep(2)

print('{0} of {1} user accounts are compromised'.format(count_pwned, count_total))